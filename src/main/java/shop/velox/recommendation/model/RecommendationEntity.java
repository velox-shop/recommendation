package shop.velox.recommendation.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import static java.time.LocalDateTime.now;

@Document(collection = "recommendation")
public class RecommendationEntity {

    @Id
    private String id;

    private String categoryId;

    private List<String> articleIds;

    private LocalDateTime recommendationTimestamp;

    public RecommendationEntity() {
        super();
        this.id = UUID.randomUUID().toString();
        recommendationTimestamp = now().withNano(0);
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getCategoryId() { return categoryId; }

    public void setCategoryId(String categoryId) { this.categoryId = categoryId; }

    public List<String> getArticleIds() { return articleIds; }

    public void setArticleIds(List<String> articleIds) { this.articleIds = articleIds; }

    public LocalDateTime getRecommendationTimestamp() { return recommendationTimestamp; }

    public void setRecommendationTimestamp(LocalDateTime recommendationTimestamp) { this.recommendationTimestamp = recommendationTimestamp; }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
