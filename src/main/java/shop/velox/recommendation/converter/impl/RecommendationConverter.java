package shop.velox.recommendation.converter.impl;

import org.mapstruct.Mapper;
import shop.velox.commons.converter.Converter;
import shop.velox.recommendation.api.dto.RecommendationServiceDto;
import shop.velox.recommendation.model.RecommendationEntity;

@Mapper
public interface RecommendationConverter extends
    Converter<RecommendationEntity, RecommendationServiceDto> {

}
