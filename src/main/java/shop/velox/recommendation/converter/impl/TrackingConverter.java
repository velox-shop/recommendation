package shop.velox.recommendation.converter.impl;

import org.mapstruct.Mapper;
import shop.velox.commons.converter.Converter;
import shop.velox.recommendation.api.dto.TrackingDto;
import shop.velox.recommendation.model.TrackingEntity;

@Mapper
public interface TrackingConverter extends Converter<TrackingEntity, TrackingDto> {

}
