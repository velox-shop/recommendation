package shop.velox.recommendation.repository.recommendation;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.mongodb.repository.MongoRepository;
import shop.velox.recommendation.model.RecommendationEntity;

import java.util.Optional;

@Tag(name = "Recommendation Repository", description = "the Recommendation Repository API")
public interface RecommendationRepository extends MongoRepository<RecommendationEntity, String> {

  Optional<RecommendationEntity> findTopByCategoryIdOrderByRecommendationTimestampDesc(String categoryId);

}
