package shop.velox.recommendation.repository.tracking;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import shop.velox.recommendation.model.TrackingEntity;
import shop.velox.recommendation.task.AggregatedLogs;
import java.util.List;

@Tag(name = "Tracking Repository", description = "the Tracking Repository API")
public interface TrackingRepository extends MongoRepository<TrackingEntity, String> {

  Page<TrackingEntity> findAllByArticleId(String article, Pageable pageable);

  Page<TrackingEntity> findAllByActionEquals(String click, Pageable pageable);

  Page<TrackingEntity> findAllByCategoryId(String category, Pageable pageable);

  Page<TrackingEntity> findAllByCartId(String cart, Pageable pageable);

  Page<TrackingEntity> findAllByOrderId(String order, Pageable pageable);

  Page<TrackingEntity> findAllBySearchTerm(String searchTerm, Pageable pageable);

  // aggregation
  @Aggregation(pipeline = {"{ $group: {_id: {articleId: $articleId, categoryId: $categoryId},  score: {$sum: 1}}}",
          "{$sort: { score: -1 }}",
          "{$project: {articleId: '$_id.articleId', categoryId: '$_id.categoryId', score: '$score'}}"})
  List<AggregatedLogs> findAllGroupedByArticleId();
}
