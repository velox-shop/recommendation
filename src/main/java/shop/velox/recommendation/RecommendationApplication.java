package shop.velox.recommendation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(
    scanBasePackages = {"shop.velox.recommendation", "shop.velox.commons"},
    exclude = {SecurityAutoConfiguration.class}
)
@EnableScheduling
@EnableMongoRepositories
public class RecommendationApplication {

  public static void main(String[] args) {
    SpringApplication.run(RecommendationApplication.class, args);
  }

}
