package shop.velox.recommendation.task;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AggregatedLogs {

	private String articleId;
	private String categoryId;
	private long score;

	public String getArticleId() { return articleId; }

	public void setArticleId(String articleId) { this.articleId = articleId; }

	public String getCategoryId() { return categoryId; }

	public void setCategoryId(String categoryId) { this.categoryId = categoryId; }

	public long getScore() { return score; }

	public void setScore(long score) { this.score = score; }

    @Override
	public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

  	@Override
    public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
