package shop.velox.recommendation.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import shop.velox.recommendation.model.RecommendationEntity;
import shop.velox.recommendation.repository.recommendation.RecommendationRepository;
import shop.velox.recommendation.repository.tracking.TrackingRepository;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class AnalyticsService {

    private static final Logger LOG = LoggerFactory.getLogger(AnalyticsService.class);

    private final TrackingRepository trackingRepository;
    private final RecommendationRepository recommendationRepository;


    public AnalyticsService(@Autowired TrackingRepository trackingRepository, @Autowired RecommendationRepository recommendationRepository) {
        this.trackingRepository = trackingRepository;
        this.recommendationRepository = recommendationRepository;
    }

    @Scheduled(fixedRate = 3600000)
    public RecommendationEntity analyzeLogs() {

        RecommendationEntity recommendationEntity = new RecommendationEntity();

        List<AggregatedLogs> aggregatedTrackingLogs = trackingRepository.findAllGroupedByArticleId();

        List<AggregatedLogs> aggregatedSortedTrackingLogs = aggregatedTrackingLogs.stream()
                .sorted(Comparator.comparing(AggregatedLogs::getScore).reversed())
                .collect(Collectors.toList());

        if(!aggregatedSortedTrackingLogs.isEmpty()) {
            List<String> articleIds = aggregatedSortedTrackingLogs.stream().map(logs -> logs.getArticleId()).collect(Collectors.toList());

            recommendationEntity.setArticleIds(articleIds);

            recommendationEntity.setCategoryId(aggregatedSortedTrackingLogs.get(0).getCategoryId());

            LOG.info("new recommendation has been generated at: " + LocalDateTime.now());

            return recommendationRepository.save(recommendationEntity);
        }
        return null;
    }
}
