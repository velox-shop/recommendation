package shop.velox.recommendation.api.dto;

import static java.time.LocalDateTime.now;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class RecommendationServiceDto {

  @Schema(description = "Unique identifier of the Recommendation.", example = "3b228r9df8-574e-694b-153a-66uu178j80i8")
  @JsonInclude
  private String id;

  @Schema(description = "Unique identifier of the Category.", example = "001")
  @JsonInclude
  private String categoryId;

  @Schema(description = "Unique identifiers of the recommended Articles.", example = "[2e5c9ba8-956e-476b-816a-49ee128a40c9, 4t7j4gy9-468t-734q-637a-50ii789a50u9, ...]")
  @JsonInclude
  private List<String> articleIds;

  @Schema(description = "Timestamp when the recommendation was generated.", example = "10-03-28 04:26:38,647")
  @JsonInclude
  private LocalDateTime recommendationTimestamp;

  public RecommendationServiceDto() {
    super();
    this.id = UUID.randomUUID().toString();
    this.recommendationTimestamp = now().withNano(0);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public List<String> getArticleIds() {
    return articleIds;
  }

  public void setArticleIds(List<String> articleIds) {
    this.articleIds = articleIds;
  }

  public LocalDateTime getRecommendationTimestamp() {
    return recommendationTimestamp;
  }

  public void setRecommendationTimestamp(LocalDateTime recommendationTimestamp) {
    this.recommendationTimestamp = recommendationTimestamp;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }

}
