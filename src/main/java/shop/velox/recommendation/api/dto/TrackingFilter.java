package shop.velox.recommendation.api.dto;

public enum TrackingFilter {
    CATEGORY,
    ARTICLE,
    CLICK,
    CART,
    ORDER,
    SEARCH,
}
