package shop.velox.recommendation.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import static java.time.LocalDateTime.now;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class TrackingDto {

  @Schema(description = "Unique identifier of the TrackingLog.", example = "3b8r9df8-574e-694b-153a-66uu178j80i8")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String  id;

  @Schema(description = "Unique identifier of the Category.", example = "12334")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String categoryId;

  @Schema(description = "Unique identifier of the Article.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String articleId;

  @Schema(description = "Unique identifier of the User.", example = "test@test.ch")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String userId;

  @Schema(description = "Unique identifier of the Search Term.", example = "notebook")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private  String searchTerm;

  @Schema(description = "Unique identifier of the action.", example = "click")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String action;

  @Schema(description = "Unique identifier of the cart.", example = "4b7e6hjA-107b-476b-816a-51hh256a30d1")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String cartId;

  @Schema(description = "Unique identifier of the order.", example = "7c8q8deA-217o-649b-517u-61rr576a68d6")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String orderId;

  //TODO: recommendation#5 (implement storing geoLocation)
  @Schema(description = "Geolocation where the tracking event was performed.", example = "current location")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String geoLocation;

  @Schema(description = "Timestamp when the tracking event occurred.", example = "10-03-28 04:26:38,647")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private LocalDateTime actionTimestamp;

  @Schema(description = "Unique identifier of the sessionId.", example = "1B5CABCD3HUY49PJDF346798A9GGK77E9")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String sessionId;

  @Schema(description = "Quantity of the ordered articled (used in conjuction with order action).", example = "23")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private BigDecimal quantity;

  @Schema(description = "Brand of the article.", example = "VELOX")
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String brand;

  public TrackingDto() {
    super();
    this.id = UUID.randomUUID().toString();
    this.actionTimestamp = now().withNano(0);
  }


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCategoryId() { return categoryId; }

  public void setCategoryId(String categoryId) { this.categoryId = categoryId; }

  public String getArticleId() { return articleId; }

  public void setArticleId(String articleId) { this.articleId = articleId; }

  public String getUserId() { return userId; }

  public void setUserId(String userId) { this.userId = userId; }

  public String getSearchTerm() {
    return searchTerm;
  }

  public void setSearchTerm(String searchTerm) {
    this.searchTerm = searchTerm;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getCartId() { return cartId; }

  public void setCartId(String cartId) { this.cartId = cartId; }

  public String getOrderId() { return orderId; }

  public void setOrderId(String orderId) { this.orderId = orderId; }

  public String getGeoLocation() {
    return geoLocation;
  }

  public void setGeoLocation(String geoLocation) {
    this.geoLocation = geoLocation;
  }

  public LocalDateTime getActionTimestamp() {
    return actionTimestamp;
  }

  public void setActionTimestamp(LocalDateTime actionTimestamp) {
    this.actionTimestamp = actionTimestamp;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }
}
