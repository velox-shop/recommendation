package shop.velox.recommendation.api.controller.impl;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.recommendation.api.controller.RecommendationController;
import shop.velox.recommendation.api.dto.RecommendationServiceDto;
import shop.velox.recommendation.converter.impl.RecommendationConverter;
import shop.velox.recommendation.model.RecommendationEntity;
import shop.velox.recommendation.service.RecommendationService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RecommendationControllerImpl implements RecommendationController {

  private final RecommendationService recommendationService;
  private final RecommendationConverter recommendationConverter;

  @Override
  public Page<RecommendationServiceDto> getRecommendations(Pageable pageable) {
    log.info("get all Recommendations");
    return recommendationConverter.convert(pageable, recommendationService.getAll(pageable));
  }

  @Override
  public ResponseEntity<RecommendationServiceDto> getRecommendationForCategory(
      String categoryId) {
    log.info("get Recommendation for category: {}", categoryId);

    Optional<RecommendationEntity> recommendationEntity = recommendationService.getRecommendationByCategory(
        categoryId);
    if (recommendationEntity.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    RecommendationServiceDto recommendationServiceDto = recommendationConverter.convertEntityToDto(
        recommendationEntity.get());
    return ResponseEntity.ok()
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS)
        .body(recommendationServiceDto);
  }

  @Override
  public ResponseEntity<RecommendationServiceDto> createRecommendation(Pageable pageable) {
    RecommendationEntity recommendationEntity = recommendationService.generateRecommendation();

    if (recommendationEntity == null) {
      return new ResponseEntity<>(HttpStatus.PRECONDITION_REQUIRED);
    }

    RecommendationServiceDto recommendationServiceDto = recommendationConverter.convertEntityToDto(
        recommendationEntity);
    return ResponseEntity.ok()
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS)
        .body(recommendationServiceDto);
  }

  @Override
  public ResponseEntity<RecommendationServiceDto> getRecommendationById(String recommendationId) {
    log.info("get Recommendation: {}", recommendationId);

    Optional<RecommendationEntity> recommendationEntity = recommendationService.getRecommendationById(
        recommendationId);
    if (recommendationEntity.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    RecommendationServiceDto recommendationServiceDto = recommendationConverter.convertEntityToDto(
        recommendationEntity.get());
    return ResponseEntity.ok()
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS)
        .body(recommendationServiceDto);
  }

}
