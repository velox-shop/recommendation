package shop.velox.recommendation.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.velox.recommendation.api.dto.RecommendationServiceDto;

@Tag(name = "Recommendation", description = "the Recommendation Service API")
@RequestMapping("/recommendations")
public interface RecommendationController {

  @Operation(summary = "gets all Recommendations", description = "Paginated.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "successful operation",
          content = @Content(schema = @Schema(implementation = RecommendationServiceDto.class)))
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  Page<RecommendationServiceDto> getRecommendations(
      @PageableDefault(size = 10) @SortDefault.SortDefaults({
          @SortDefault(sort = "recommendationTimestamp", direction = Sort.Direction.DESC)}) Pageable pageable);


  @Operation(summary = "gets Recommendation for category", description = "Paginated.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "successful operation",
          content = @Content(schema = @Schema(implementation = RecommendationServiceDto.class)))
  })
  @GetMapping(value = "/categories/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<RecommendationServiceDto> getRecommendationForCategory(
      @Parameter(description = "Category of articles for the Recommendation.", required = true) @PathVariable("categoryId") String categoryId);


  @Operation(summary = "create Recommendations for category", description = "Paginated.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "successful operation",
          content = @Content(schema = @Schema(implementation = RecommendationServiceDto.class)))
  })
  @PostMapping(value = "/analyze", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<RecommendationServiceDto> createRecommendation(
      @PageableDefault(size = 10) Pageable pageable);


  @Operation(summary = "Retrieves a Recommendation by its id", description = "Retrieves a Recommendation")
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "Tracking Entry is found",
          content = @Content(schema = @Schema(implementation = RecommendationServiceDto.class))),
      @ApiResponse(responseCode = "404",
          description = "Tracking Entry not found",
          content = @Content(schema = @Schema()))
  })
  ResponseEntity<RecommendationServiceDto> getRecommendationById(
      @Parameter(description = "Id of the Recommendation. Cannot be empty.", required = true) @PathVariable("id") String recommendationId);

}
