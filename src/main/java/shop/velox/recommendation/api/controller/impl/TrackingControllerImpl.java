package shop.velox.recommendation.api.controller.impl;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.recommendation.api.controller.TrackingController;
import shop.velox.recommendation.api.dto.TrackingDto;
import shop.velox.recommendation.api.dto.TrackingFilter;
import shop.velox.recommendation.converter.impl.TrackingConverter;
import shop.velox.recommendation.model.TrackingEntity;
import shop.velox.recommendation.service.TrackingService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class TrackingControllerImpl implements TrackingController {


  private final TrackingService trackingService;
  private final TrackingConverter trackingConverter;

  @Override
  public ResponseEntity<TrackingDto> createTrackingEntry(final TrackingDto trackingDto) {
    TrackingEntity trackingEntity = trackingConverter.convertDtoToEntity(trackingDto);
    TrackingDto createdTrackingDto = trackingConverter.convertEntityToDto(
        trackingService.createTrackingEntry(trackingEntity));
    log.info("created tracking entry dto {}:", createdTrackingDto);

    return ResponseEntity
        .status(HttpStatus.CREATED)
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS)
        .body(createdTrackingDto);
  }

  @Override
  public Page<TrackingDto> getTrackingEntries(Pageable pageable, final TrackingFilter filter,
      final String trackingSelector) {
    log.info("get all Tracking Entries for filter: {} and selector: {}", filter, trackingSelector);
    return trackingConverter.convert(pageable,
        trackingService.getAll(pageable, filter, trackingSelector));
  }

  @Override
  public ResponseEntity<TrackingDto> getTrackingEntry(final String trackingId) {
    log.info("get Tracking Entry: {}", trackingId);

    Optional<TrackingEntity> trackingEntity = trackingService.getTrackingEntry(trackingId);
    if (trackingEntity.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    TrackingDto trackingDto = trackingConverter.convertEntityToDto(trackingEntity.get());

    return ResponseEntity.ok()
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS)
        .body(trackingDto);
  }
}
