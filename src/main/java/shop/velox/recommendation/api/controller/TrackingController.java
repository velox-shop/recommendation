package shop.velox.recommendation.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import shop.velox.recommendation.api.dto.TrackingDto;
import shop.velox.recommendation.api.dto.TrackingFilter;

@Tag(name = "Tracking", description = "the Tracking Service API")
@RequestMapping("/trackings")
public interface TrackingController {

    String TRACKING_FILTER = "trackingFilter";
    String TRACKING_SELECTOR = "trackingSelector";

    @Operation(summary = "Creates a Tracking Entry", description = "")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Tracking Entry created.",
                    content = @Content(schema = @Schema(implementation = TrackingDto.class))),
    })
    ResponseEntity<TrackingDto> createTrackingEntry(
            @Parameter(description = "Tracking Entry.") @RequestBody TrackingDto trackingDto
    );


    @Operation(summary = "gets all Tracking Entries (optional: filtered by some criteria)", description = "Paginated.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "successful operation",
                    content = @Content(schema = @Schema(implementation = TrackingDto.class)))
    })
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    Page<TrackingDto> getTrackingEntries(
            @PageableDefault(size = 10) @SortDefault.SortDefaults({
                    @SortDefault(sort = "actionTimestamp", direction = Sort.Direction.DESC)}) Pageable pageable,
            @Parameter(description = "Return tracking logs. If filter is empty, return all tracking logs, otherwise perform filter. Can be empty.")
            @RequestParam(name = TRACKING_FILTER, required = false) final TrackingFilter filter,
            @Parameter(description = "Tracking selector (category, article, cart, order, ...). Mandatory if tracking filter is present. Can be empty.")
            @RequestParam(name = TRACKING_SELECTOR, required = false) final String trackingSelector
    );


    @Operation(summary = "Retrieves a Tracking Entry", description = "Retrieves a Tracking Entry")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Tracking Entry is found",
                    content = @Content(schema = @Schema(implementation = TrackingDto.class))),
            @ApiResponse(responseCode = "404",
                    description = "Tracking Entry not found",
                    content = @Content(schema = @Schema()))
    })
    ResponseEntity<TrackingDto> getTrackingEntry(
            @Parameter(description = "Id of the Tracking Entry. Cannot be empty.", required = true) @PathVariable("id") String trackingId);
}
