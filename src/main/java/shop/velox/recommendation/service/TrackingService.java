package shop.velox.recommendation.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.recommendation.api.dto.TrackingFilter;
import shop.velox.recommendation.model.TrackingEntity;

import java.util.Optional;

public interface TrackingService {

    TrackingEntity createTrackingEntry(TrackingEntity trackingEntity);

    Page<TrackingEntity> getAll(Pageable pageable, TrackingFilter filter, String trackingSelector);

    Optional<TrackingEntity> getTrackingEntry(String trackingId);
  
}
