package shop.velox.recommendation.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.recommendation.model.RecommendationEntity;

import java.util.Optional;

public interface RecommendationService {

    Page<RecommendationEntity> getAll(Pageable pageable);

    Optional<RecommendationEntity> getRecommendationByCategory(String categoryId);

    Optional<RecommendationEntity> getRecommendationById(String recommendationId);

    RecommendationEntity generateRecommendation();
}
