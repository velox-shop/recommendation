package shop.velox.recommendation.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.recommendation.api.dto.TrackingFilter;
import shop.velox.recommendation.model.TrackingEntity;
import shop.velox.recommendation.repository.tracking.TrackingRepository;
import shop.velox.recommendation.service.TrackingService;

import java.util.Optional;

@Component
public class TrackingServiceImpl implements TrackingService {

    private final TrackingRepository trackingRepository;

    @Autowired
    public TrackingServiceImpl(TrackingRepository trackingRepository) {
        this.trackingRepository = trackingRepository;
    }

    @Override
    public TrackingEntity createTrackingEntry(TrackingEntity trackingEntity) {
        return trackingRepository.save(trackingEntity);
    }

    @Override
    public Page<TrackingEntity> getAll(Pageable pageable, TrackingFilter filter, String trackingSelector) {
        if (filter == null) {
            return trackingRepository.findAll(pageable);
        } else {
            if (filter.equals(TrackingFilter.CLICK)) {
                return trackingRepository.findAllByActionEquals("click", pageable);
            }
            if (StringUtils.isNotBlank(trackingSelector)) {
                if (filter.equals(TrackingFilter.ARTICLE)) {
                    return trackingRepository.findAllByArticleId(trackingSelector, pageable);
                } else if (filter.equals(TrackingFilter.CATEGORY)) {
                    return trackingRepository.findAllByCategoryId(trackingSelector, pageable);
                } else if (filter.equals(TrackingFilter.CART)) {
                    return trackingRepository.findAllByCartId(trackingSelector, pageable);
                } else if (filter.equals(TrackingFilter.ORDER)) {
                    return trackingRepository.findAllByOrderId(trackingSelector, pageable);
                } else {
                    return trackingRepository.findAllBySearchTerm(trackingSelector, pageable);
                }
            } else {
                throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "tracking selector (category, article, cart, order, ...) is not present");
            }
        }
    }

    @Override
    public Optional<TrackingEntity> getTrackingEntry(String trackingId) {
        return trackingRepository.findById(trackingId);
    }
}
