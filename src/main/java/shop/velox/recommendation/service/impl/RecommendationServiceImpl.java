package shop.velox.recommendation.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import shop.velox.recommendation.model.RecommendationEntity;
import shop.velox.recommendation.repository.recommendation.RecommendationRepository;
import shop.velox.recommendation.service.RecommendationService;
import shop.velox.recommendation.task.AnalyticsService;

import java.util.Optional;

@Component
public class RecommendationServiceImpl implements RecommendationService {

    private final RecommendationRepository recommendationRepository;
    private final AnalyticsService analyticsService;

    @Autowired
    public RecommendationServiceImpl(RecommendationRepository recommendationRepository, AnalyticsService analyticsService) {
        this.recommendationRepository = recommendationRepository;
        this.analyticsService = analyticsService;
    }

    @Override
    public Page<RecommendationEntity> getAll(Pageable pageable) {
        return recommendationRepository.findAll(pageable);
    }

    @Override
    public Optional<RecommendationEntity> getRecommendationByCategory(String categoryId) {
        return recommendationRepository.findTopByCategoryIdOrderByRecommendationTimestampDesc(categoryId);
    }

    @Override
    public Optional<RecommendationEntity> getRecommendationById(String recommendationId) {
        return recommendationRepository.findById(recommendationId);
    }

    @Override
    public RecommendationEntity generateRecommendation() {
        return analyticsService.analyzeLogs();
    }
}
