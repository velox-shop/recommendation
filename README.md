# Recommendation Module
This repository contains the Mongo database for storing tracking information and articles recommendation.

More information about the recommendation module can be found on the project [wiki](https://gitlab.com/velox-shop/recommendation/-/wikis/home) page.

## Work locally
Build Service
```
./gradlew clean build
```

Execute with Gradle:
```
./gradlew bootRun
```

Execute with Docker Compose
```
docker-compose stop && docker-compose rm -fv && docker-compose up --build --force-recreate --remove-orphans
```

Test
```
newman run src/test/*postman_collection.json --environment src/test/veloxRecommendation-local.postman_environment.json --reporters cli,html --reporter-html-export newman-results.html
```

## API documentation

Online API Documentation is at:

- HTML: http://localhost:8452/recommendation/v1/swagger-ui.html
- JSON: http://localhost:8452/recommendation/v1/api-docs

Offline API Documentation is at https://velox-shop.gitlab.io/recommendation/


## Tracking

### Example Tracking JSON for on click action (pdp -> plp or search result)

```
{
  "categoryId": "001",
  "articleId": "2e5c9ba8-956e-476b-816a-49ee128a40c9",
  "userId": "test@test.ch",
  "action": "click",
  "geolocation": {
                    "location": {
                        "lat": 37.421875199999995,
                        "lng": -122.0851173
                    },
                    "accuracy": 120
                 },
  "timestamp": "2021-09-22T10:01:10",        
  "sessionId": "1B5CABCD3HUY49PJDF346798A9GGK77E9",  
  "brand": "VELOX",   
}
```

### Example Tracking JSON for add to cart action

```
{
  "categoryId": "001",
  "articleId": "2e5c9ba8-956e-476b-816a-49ee128a40c9",
  "userId": "test@test.ch",
  "action": "add_to_cart",
  "cartId": "4b7e6hjA-107b-476b-816a-51hh256a30d1",
  "geolocation": {
                    "location": {
                        "lat": 37.421875199999995,
                        "lng": -122.0851173
                    },
                    "accuracy": 120
                 },
  "timestamp": "2021-09-22T10:01:10",        
  "sessionId": "1B5CABCD3HUY49PJDF346798A9GGK77E9",  
  "brand": "VELOX",   
}
```

### Example Tracking JSON for order action

```
{
  "categoryId": "001",
  "articleId": "2e5c9ba8-956e-476b-816a-49ee128a40c9",
  "userId": "test@test.ch",
  "action": "order",
  "orderId": "7c8q8deA-217o-649b-517u-61rr576a68d6",
  "geolocation": {
                    "location": {
                        "lat": 37.421875199999995,
                        "lng": -122.0851173
                    },
                    "accuracy": 120
                 },
  "timestamp": "2021-09-22T10:01:10",        
  "sessionId": "1B5CABCD3HUY49PJDF346798A9GGK77E9",  
  "quantity": "20", 
  "brand": "VELOX",   
}
```

### Example Tracking JSON for search action

```
{
  "userId": "anonymous",
  "search_term": "notebook", 
  "action": "search",
  "geolocation": {
                    "location": {
                        "lat": 37.421875199999995,
                        "lng": -122.0851173
                    },
                    "accuracy": 120
                 },
  "timestamp": "2021-09-22T10:01:10",        
  "sessionId": "1B5CABCD3HUY49PJDF346798A9GGK77E9",   
}
```
## Recommendation

### Example Recommendation JSON for cart scope

 ```
{
    "searchScope": "cart",
    "categoryId": "001",
    "articles": "[2e5c9ba8-956e-476b-816a-49ee128a40c9, 4t7j4gy9-468t-734q-637a-50ii789a50u9, ...]",
    "actionTimestamp": "2021-09-22T12:01:09",
    
}

```
### Recommendation Service images

|Version      | Frontend                                                                                                                                             |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1.0.0 (1.0) | Base image (base tracking and generation of recommendations, all articles are in the same category)                                                  |
| 1.1.0       | Add endpoint to get the latest recommendation for category                                                                                           |
